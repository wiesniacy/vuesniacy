import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Rbout from './views/Rules.vue'
import Character from './views/Character.vue'
import Login from './views/Login.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/rules',
      name: 'rules',
      component: Rbout
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      //component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/character',
      name: 'character',
      component: Character
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})
